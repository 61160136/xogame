import java.util.Scanner;
public class xo {

	public static void showWelcome(){
		System.out.println("|--->>WELCOME TO OX GAME<<---|");}
	
	public static char[][] table = {
			{'-', '-', '-'},
			{'-', '-', '-'},
			{'-', '-', '-'}};
	
    public static void showTable() {
    	 for(int i=0;i<table.length;i++){
             for(int j=0;j<table.length;j++){
                 System.out.print(table[i][j]);
             }
             System.out.println();
             }}
    
    public static char oxTurn = 'X';
    
	public static void showTurn(){
		System.out.println(oxTurn+" "+"Turn");}
		
	public static void showInput(char[][] table, char oxTurn) {
		Scanner kb = new Scanner(System.in);
		System.out.print("Please input Row|Column : ");
		int row = kb.nextInt();
		int col = kb.nextInt();
		for(;;) {
			if(table[row-1][col-1]=='-') {
				table[row-1][col-1] = oxTurn;
				break;
			}else {
				System.out.println("NOT NULL!! PLEASE INPUT AGAIN");
				System.out.print("Please input Row|Column: ");
				row = kb.nextInt();
				col = kb.nextInt();
			}
			}}
	
	public static void checkTurn() {
		if(oxTurn == 'O'){
			oxTurn = 'X';
        }else {
        	oxTurn = 'O';
        }}
	
	public static boolean checkRow(char[][] table) {
		if(table[0][0] == table[0][1] && table[0][1] == table[0][2] && table[0][0] != '-') {
			showWinner();
			return true;	
		}else if(table[1][0] == table[1][1] && table[1][1] == table[1][2] && table[1][0] != '-') {
			showWinner();
			return true;
		}else if(table[2][0] == table[2][1] && table[2][1] == table[2][2] && table[2][0] != '-') {
			showWinner();
			return true;
		}else {
			return false;
		}
	}
	
	public static boolean checkColumn(char[][] table) {
		if(table[0][0] == table[1][0] && table[1][0] == table[2][0] && table[0][0] != '-') {
			showWinner();
			return true;	
		}else if(table[0][1] == table[1][1] && table[1][1] == table[2][1] && table[0][1] != '-') {
			showWinner();
			return true;
		}else if(table[0][2] == table[1][2] && table[1][2] == table[2][2] && table[0][2] != '-') {
			showWinner();
			return true;
		}else {
			return false;
		}
	}
	
	public static boolean checkDiag(char[][] table) { 
		if(table[0][0] == table[1][1] && table[1][1] == table[2][2] && table[0][0] != '-') {
			showWinner();
			return true;	
		}else if(table[2][0] == table[1][1] && table[1][1] == table[0][2] && table[2][0] != '-') {
			showWinner();
			return true;
		}else {
			return false;
		}
	}
	
	public static boolean checkDraw(char[][] table) {
		int n = 0;
		for(int i=0; i<3; i++) {
			for(int j=0; j<3; j++) {
				if(table[i][j] != '-') {
					n++;
				}}}
		if(n==9) {
			showTable();
			System.out.println("<------- TIE GAME!!! -------->");
			return true;
		}else {
			return false;
		}
	}
	
	static void showWinner() {
		for ( int i = 0; i < 3; i++ ) {
			for ( int j = 0; j < 3; j++ ) {				
				System.out.print(table[i][j]);				
			}
			System.out.println();
	}
		System.out.println("<----"+" "+oxTurn +" "+"THE WINNER!! ---->");
	}
	
	 public static void showGoodbye() {		
		System.out.println("|------------------------|");
    	System.out.println("<--------GOOD BYE-------->");
	}
	
    public static void main(String[] arg){  	
    	showWelcome();    	
    	while(true) {
    		showTable();
    		showTurn();
    		showInput(table,oxTurn);
    	if(checkRow(table) == true || checkColumn(table) == true || checkDiag(table) == true || checkDraw(table)==true ) {
    		break;
    	}  
    	checkTurn(); 
        }
    	showGoodbye();   	
	}

}
